FROM node:9-slim
WORKDIR /script
COPY package.json /script
RUN npm install
COPY . /script
CMD ["npm","start"]